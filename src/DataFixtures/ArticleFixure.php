<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;
use App\Entity\User;


class ArticleFixure extends Fixture 
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 5; $i++) {
            $article = new Article();
            $article->setCategory("divers");
            $article->setDescription("this is a description !");
            $article->setDisponibility(true);
            $article->setImage("image");
            $article->setName("product ${i}");
            $article->setPrice(100*$i);
            $article->setStock(10);
            $manager->persist($article);
        }

        $manager->flush();
    }
}
