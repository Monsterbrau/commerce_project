<?php

namespace App\Service;

use App\Entity\LineCart;
use App\Entity\User;
use App\Entity\ShoppingCart;
use App\Entity\Article;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\Common\Persistence\ObjectManager;


class AddProductService
{
  private $user;
  private $manager;

  public function __construct(TokenStorageInterface $token, ObjectManager $manager)
  {
    $this->user = $token->getToken()->getUser();
    $this->manager = $manager;
  }

  public function addArticleToCart(Article $article)
  {
    $shoppingCarts = $this->user->getShoppingCarts();
    $shoppingCart = null;
    foreach ($shoppingCarts as $loop) {
      if (!$loop->getOrder()) {
        $shoppingCart = $loop;
        break;
      }

    }

    $lines = $shoppingCart->getLineCart();
    $lineCart = null;
    foreach ($lines as $loopline) {
      if ($article->getId() === $loopline->getIdProduct()->getId()) {
        $lineCart = $loopline;
        $lineCart->setQuantity($lineCart->getQuantity() + 1);
        break;
      }
    }

    if (!$lineCart) {
      $lineCart = new LineCart();
      $lineCart->setIdProduct($article);
      $lineCart->setPrice($article->getPrice());
      $lineCart->setQuantity(1);
      $shoppingCart->addLineCart($lineCart);
    }
    $shoppingCart->setTotal(0);
    foreach ($lines as $loopline) {
      $shoppingCart->setTotal($shoppingCart->getTotal() + ($loopline->getPrice() * $loopline->getQuantity()));
    }
    $this->manager->persist($shoppingCart);
    $this->manager->flush();
  }

  public function removeArticleToCart(Article $article)
  {
    $shoppingCarts = $this->user->getShoppingCarts();
    $shoppingCart = null;
    foreach ($shoppingCarts as $loop) {
      if (!$loop->getOrder()) {
        $shoppingCart = $loop;
        break;
      }
    }

    $lines = $shoppingCart->getLineCart();
    $lineCart = null;
    foreach ($lines as $loopline) {
      if ($article->getId() === $loopline->getIdProduct()->getId()) {
        $lineCart = $loopline;
        $lineCart->setQuantity($lineCart->getQuantity() - 1);
        $shoppingCart->setTotal($shoppingCart->getTotal() + ($lineCart->getPrice() * $lineCart->getQuantity()));
        break;

      }
    }
    if ($lineCart->getIdProduct()->getQuantity() === 0) {
      $shoppingCart->removeLineCart($lineCart);
    }

    foreach ($lines as $loopline) {
      $shoppingCart->setTotal($shoppingCart->getTotal() + ($lineCart->getPrice() * $lineCart->getQuantity()));
    }
    $shoppingCart->setTotal(0);
    foreach ($lines as $loopline) {
      $shoppingCart->setTotal($shoppingCart->getTotal() + ($loopline->getPrice() * $loopline->getQuantity()));
    }
    $this->manager->persist($shoppingCart);
    $this->manager->flush();
  }

  public function orderShoppingCart()
  {
    $shoppingCarts = $this->user->getShoppingCarts();
    foreach ($shoppingCarts as $shoppingCart) {
      if ($shoppingCart->getOrder() === false) {
        $shoppingCart->setOrder(true);
        $this->manager->persist($shoppingCart);
        $this->user->addShoppingCart(new ShoppingCart());
        $this->manager->persist($shoppingCarts);
        break;
      }
    }
    $this->manager->flush();
  }
}



