<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180831131859 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE shopping_cart ADD linecarts_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shopping_cart ADD CONSTRAINT FK_72AAD4F67D59A6EF FOREIGN KEY (linecarts_id) REFERENCES line_cart (id)');
        $this->addSql('CREATE INDEX IDX_72AAD4F67D59A6EF ON shopping_cart (linecarts_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE shopping_cart DROP FOREIGN KEY FK_72AAD4F67D59A6EF');
        $this->addSql('DROP INDEX IDX_72AAD4F67D59A6EF ON shopping_cart');
        $this->addSql('ALTER TABLE shopping_cart DROP linecarts_id');
    }
}
