<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\Collection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="boolean")
     */
    private $admin;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShoppingCart", mappedBy="user", orphanRemoval=true, cascade={"persist"})
     */
    private $shoppingCarts;

    public function __construct()
    {
        $this->admin = false;
        $this->shoppingCarts = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName() : ? string
    {
        return $this->name;
    }

    public function setName(string $name) : self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname() : ? string
    {
        return $this->surname;
    }

    public function setSurname(string $surname) : self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getEmail() : ? string
    {
        return $this->email;
    }

    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword() : ? string
    {
        return $this->password;
    }

    public function setPassword(string $password) : self
    {
        $this->password = $password;

        return $this;
    }


    public function getAddress() : ? string
    {
        return $this->address;
    }

    public function setAddress(string $address) : self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhoneNumber() : ? string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber) : self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getAdmin() : ? bool
    {
        return $this->admin;
    }

    public function setAdmin(bool $admin) : self
    {
        $this->admin = $admin;

        return $this;
    }

    public function getRoles()
    {
        if ($this->getAdmin()) {
            return array('ROLE_ADMIN');
        } else {
            return array('ROLE_USER');
        }
    }

    public function addRoles(string $roles)
    {
        $this->roles = $roles;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getSalt()
    {

    }

    public function eraseCredentials()
    {

    }
    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->name,
            $this->surname,
            $this->email,
            $this->password,
            $this->address,
            $this->phoneNumber,
            $this->admin,
            // see section on salt below
            // $this->salt,
        ));
    }
    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->name,
            $this->surname,
            $this->email,
            $this->password,
            $this->address,
            $this->phoneNumber,
            $this->admin,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|ShoppingCart[]
     */
    public function getShoppingCarts(): \Doctrine\Common\Collections\Collection
    {
        return $this->shoppingCarts;
    }

    public function addShoppingCart(ShoppingCart $shoppingCart): self
    {
        if (!$this->shoppingCarts->contains($shoppingCart)) {
            $this->shoppingCarts[] = $shoppingCart;
            $shoppingCart->setUser($this);
        }

        return $this;
    }

    public function removeShoppingCart(ShoppingCart $shoppingCart): self
    {
        if ($this->shoppingCarts->contains($shoppingCart)) {
            $this->shoppingCarts->removeElement($shoppingCart);
            // set the owning side to null (unless already changed)
            if ($shoppingCart->getUser() === $this) {
                $shoppingCart->setUser(null);
            }
        }

        return $this;
    }
}
