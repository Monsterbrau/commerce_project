<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\ArticleType;
use App\Service\FileUploader;

class ArticleController extends Controller
{

    /**
     * @Route("/article/{id}", name="article")
     */
    public function index(ArticleRepository $repo, int $id)
    {
        $article = $repo->find($id);

        return $this->render('article/index.html.twig', [
            'controller_name' => 'ArticleController',
            'article' => $article,
        ]);
    }

    /**
     * @Route("/admin/articleCreation", name="articleCreation")
     * @Route("/admin/articleUpdate/{id}", name="articleUpdate")
     */
    public function articleManage(Request $request, Article $article = null, ObjectManager $manager, FileUploader $fileUploader)
    {
        if (!$article) {
            $article = new Article();
        }

        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {
            
            
            $file = $article->getImage();
            $fileName = $fileUploader->upload($file);

            $article->setImage($fileName);
            $manager->persist($article);
            $manager->flush();
        
            // return $this->redirect($this->generateUrl('app_article_list')) && $this->redirectToRoute('home');
            return $this->redirectToRoute('home');
        }




    return $this->render('articleCreation/index.html.twig', [
        'controller_name' => 'GestionAddController',
        'form' => $form->createView(),
    ]);

}

/**
 * @return string
 */
private function generateUniqueFileName()
{
    return md5(uniqid());
}

/**
 * @Route("/admin/articleDelete/{id}", name="articleDelete")
 */
public function delete(ArticleRepository $repo, int $id)
{
    $removeArticle = $repo->find($id);
    $em = $this->getDoctrine()->getManager();
    $em->remove($removeArticle);
    $em->flush();

    return $this->redirectToRoute('home');
}
}
