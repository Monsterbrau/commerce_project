<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\UserType;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use App\Service\AddProductService;
use App\Entity\Article;
use App\Entity\ShoppingCart;
use App\Repository\ShoppingCartRepository;
use App\Repository\LineCartRepository;

class HomeController extends Controller
{


    /**
     * @Route("/", name="home")
     */
    public function index(ArticleRepository $repo)
    {

        $result = $repo->findAll();

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'result' => $result,
        ]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $lastMail = $utils->getLastUsername();

        return $this->render('login/index.html.twig', [
            'error' => $error,
            'lastEmail' => $lastMail
        ]);
    }

    /**
     * @Route("/sign", name="sign_up")
     */
    public function signUp(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $form = $this->createForm(UserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $shoppingCart = new ShoppingCart();
            $shoppingCart->setOrder(false);
            $shoppingCart->setTotal(0);

            $user->addShoppingCart($shoppingCart);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('home');
        }



        return $this->render(
            'sign_up/index.html.twig',
            [
                'form' => $form->createView(),

            ]
        );
    }

    /**
     * @Route("/profile/{id}", name="profile")
     */
    public function profile(Request $request, ObjectManager $manager, UserRepository $repo, int $id, UserPasswordEncoderInterface $encoder)
    {
        $user = $repo->find($id);
        $form = $this->createForm(UserType::class, $user);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('profile/index.html.twig', [
            'controller_name' => 'HomeController',
            'form' => $form->createView(),
            'id' => $id
        ]);
    }

    /**
     * @Route("/addArticleToCart/{article}", name="add")
     */
    public function add(AddProductService $add, Article $article)
    {

        $add->addArticleToCart($article);
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/removeArticleToCart/{article}", name="remove")
     */
    public function removeArticle(AddProductService $remove, Article $article)
    {
        $remove->removeArticleToCart($article);
        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/order/", name="order")
     */

    public function orderCart(AddProductService $order,ShoppingCartRepository $repo)
    {
        $order->orderShoppingCart($repo);
        return $this->redirectToRoute('home');
    }
}

